const ChatApp = require('./chat');
const {chatOnMessage, readyChatOnMessage, onVkCloseHandler} = require('./event-handlers');

const WEBINAR_ON_MESSAGE_TIMEOUT = 30000;
const VK_CLOSE_TIMEOUT = 10000;
const FB_CLOSE_TIMEOUT = 15000;

let webinarChat =  new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat =       new ChatApp('---------vk');
vkChat.setMaxListeners(2);

webinarChat.on('message', chatOnMessage);
webinarChat.on('message', readyChatOnMessage);
facebookChat.on('message', chatOnMessage);
vkChat.on('message', chatOnMessage);
vkChat.on('message', readyChatOnMessage);
vkChat.on('close', onVkCloseHandler);

//Отписать чат вебинар от chatOnMessage
setTimeout(() => {
    webinarChat.removeListener('message', chatOnMessage);
}, WEBINAR_ON_MESSAGE_TIMEOUT);

// Закрыть вконтакте
setTimeout( ()=> {
  console.log('Закрываю вконтакте...');
  vkChat.removeAllListeners('message', chatOnMessage);
  vkChat.close();
}, VK_CLOSE_TIMEOUT );

// Закрыть фейсбук
setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
  facebookChat.removeListener('message', chatOnMessage);
}, FB_CLOSE_TIMEOUT );

/*
* Задания:
*
* 1)
*  1.1 Добавить обработчик события `message` для Чата Вебинара (`webinarChat`),
*      который выводит в консоль сообщение 'Готовлюсь к ответу'.
*      Обработчик создать в отдельной функции.
*
*  1.2 Для вконтакте (`vkChat`) установить максимальное количество обработчиков событий, равное 2.
*
*  1.3 Добавить обработчик 'Готовлюсь к ответу' из пп. 1.1 к чату вконтакте.
*
*
* 2)
*
*  2.1 В классе чата `ChatApp` добавить метод `close`, который будет вызывать событие `close`
*      (событие вызывается один раз, `setInterval` как в констукторе, не нужен).
*
*  2.2 Для чата вконтакте (`vkChat`) добавить обработчик `close`,
*      выводящий в консоль текст  "Чат вконтакте закрылся :(".
*
*  2.3 Вызывать у чата вконтакте метод `close()`.
*
* 3)
*   Добавить код, который через 30 секунд отписывает `chatOnMessage` от вебинара `webinarChat`.
*
*   Задание со звездочкой (дополнительное) —
*   разбить существующий код на модули, запускаемый файл должен быть `index.js`
*
*
*
* Спасибо!
*
* */
